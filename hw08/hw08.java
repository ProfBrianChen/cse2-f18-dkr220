//Daniel Reph
//CSE 002-310
//November 10, 2018
//HW 08: Shuffling
//This program will shuffle cards and then output a hand
//import necessary packages
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class hw08{ 
public static void main(String[] args) { 
  //declare scanner
  Scanner scan = new Scanner(System.in); 
  //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};   
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
  int numCards = 5; 
  int again = 1; 
  int index = 51; 
  //create deck of cards
  System.out.println("CARDS:");
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      //print each card out
      System.out.print(cards[i]+" "); 
    }
  System.out.println(); 
  //
  //printArray(cards); 
  System.out.println("SHUFFLED:");
  shuffle(cards); 
  //printArray(cards); 
  while(again == 1){ 
    //print out what the hand is going to be
    System.out.println("Hand:");
    //call method to get hand
    hand = getHand(cards,index,numCards); 
    //System.out.println(Arrays.toString(hand));
    //printArray(hand);
    //if the index is less than number of cards, get new deck
    if(index < numCards){
      //print out what the new deck will be
      System.out.println("New Deck: ");
      //call shuffle method for new deck
      shuffle(cards);
      //reset index
      index = 51;
    }
    //adjust index after each hand
    index = index - numCards;
    //ask user if they want another hand
    System.out.println("Enter a 1 if you want another hand drawn"); 
    //take in int, if 1 go again
    again = scan.nextInt(); 
  }  
}//end main
  //print array method
  public static void printArray(String list[]){
    for(int i = 0; i < list.length; i++){
      //System.out.print(list[i]+ " ");
      
    }
  }//end print array method
  //shuffle method
  public static void shuffle(String list[]){
    //declare number generator
    Random randomGenerator = new Random();
    for(int i = 0; i < 52; i++){
      //make temp variable
      String[] temp = new String[1];
      //get random number
      int x = randomGenerator.nextInt(52);
      //make second temp variable
      String temp2 = list[i];
      //switch arrays 
      list[i] = list[x];
      list[x] = temp2;
    }//print out array
    for(int i = 0; i < 52; i++){
      System.out.print(list[i] + " ");
      //System.out.print(Arrays.toString(list));
    }
      System.out.println();
  }//get hand method
  public static String[] getHand(String cards[], int index, int numCards){
    //System.out.println(index);
    //System.out.println(numCards);
    //reset index if less than numcards
    if(index < numCards){
      index = 51;
    }//create hand for numCards
    for(int i = 0; i < numCards; i++){
      cards[i] = cards[index];
      //print out each hand
      System.out.print(cards[i] + " ");
      //decrease index
      index--;
    }  System.out.println();
    //return hand
    return cards;
  }
}//end class