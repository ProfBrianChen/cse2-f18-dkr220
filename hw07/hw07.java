//Daniel Reph
//CSE 002-310
//October 26 2018
//HW 07
//This program will take user input and give them feedback and options to edit

//first import the scanner
import java.util.Scanner;


public class hw07{   
  public static void main(String ars[]){
    //decalre and initialize the scanner
    Scanner myScanner = new Scanner(System.in);
    //declare a string for the input
    String inputText = sampleText();
    //ask the user for an input
    System.out.println("your input text is: " + inputText);
    //create a boolean for while loop    
    boolean keepGoing = true;
    //begin while loop
    while(keepGoing == true){
      //declare string for their choice
      String editChoice = printMenu();
      //ask the user what they want to do
      System.out.println("your choice is: " + editChoice); 
      //if chosen c
      if(editChoice.equals("c")){
        //get nonwhitespace characters by using method
        //declare variable and call method
        int numberNonWhiteSpaceCharacters = getNumOfNonWSCharacters(inputText);
      //keepGoing = true;
    }
    if(editChoice.equals("w")){
      //if chosen w, call w's method
      int numberOfWords = getNumOfWords(inputText);
      //keepGoing = true;
    }
    if(editChoice.equals("f")){
      //if chosen f, call f's method
      int numberOfInstances = findText(inputText);
      //keepGoing = true;
    }
    if(editChoice.equals("r")){
      //if chosen r, call r's method
      String replacedText = replaceExclamation(inputText);
      //keepGoing = true;
    }
    if(editChoice.equals("s")){
      //if chosen s, call s's method
      String newText = shortenSpace(inputText); 
      //keepGoing = true;
    }
      if(editChoice.equals("q")){
        //if chosen q, set boolean to false so while loop stops
        //and program ends
        keepGoing = false;
      }
    }
  }//end main method
  //method for getting input text
  public static String sampleText(){
    //initialize scanner
    Scanner myScanner = new Scanner(System.in);
    //ask user for string
    System.out.println("Enter a string of your choosing");
    //declare and initialze their input
    String input = myScanner.nextLine();
    //repeat back to the user what they entered
    System.out.println("You entered: " + input);
    //return their input back to the main method
    return input;
  }//end sampleText method
  //method for giving user a menu to pick an option
  public static String printMenu(){
    //initialize scanner
    Scanner myScanner = new Scanner(System.in);
    //make boolean
    boolean x = false;
    //initialize string
    String menuChoice = "";
    //begin while loop
    while (x == false){
      //print out the menu until they hit q
      //ask user for their choice
    System.out.printf("MENU:\n c - number of non-whitespace characters\n w - number of words\n" +
                      " f - find text\n r - replace all !'s\n s - shorten spaces\n q - quit\n");
      //take their input choice and put it into variable
    menuChoice = myScanner.next();
      //System.out.println("menuChoice: " + menuChoice);
      //check to see if their input is valid
      if(menuChoice.equals("c") || menuChoice.equals("w") || menuChoice.equals("f") || menuChoice.equals("r") ||
        menuChoice.equals("s") || menuChoice.equals("q")){
        x = true;
        menuChoice = menuChoice;
      }//end if
      else{
        //if not valid, tell them, and make them pick again
        System.out.println("Invalid entry");
        menuChoice = menuChoice;
        x = false;
      }//end else
    }//end while loop
    //return their valid entry back to the main method
    return menuChoice;
    
    
  }//end printMenu method
  //method for num of WS characters
  public static int getNumOfNonWSCharacters(String input){
    //delcare and initialize counters
    int nonBlankCounter = 0;
    int i = 0;
    //System.out.println(input.length());
    //make while loop
    while(input.length() > i){
      if ( input.charAt( i ) != ' ' ){
        //if characters doesn't equal space, increase counter
        nonBlankCounter++;
      }
      //increase counter after each space value
      i++;
    }   
    //print out how many non blanks
    System.out.println("non blanks: " + nonBlankCounter);
    //return value to main method
    return nonBlankCounter;
  }//end getNumOfNonWSCharacters method
  //method for number of words
  public static int getNumOfWords(String input){
    //declare and initialize word count, make boolean, and where line ends
    int wordCount = 0;
    boolean word = false;
    int endOfLine = input.length() - 1;
    //create for loop
    for (int i = 0; i < input.length(); i++) {
      //if the character is a letter
      if (Character.isLetter(input.charAt(i)) && i != endOfLine) {
        word = true;
        //if character isn't a letter counter goes up.
      } 
      //last word of string
      else if (!Character.isLetter(input.charAt(i)) && word) {
        wordCount++;           
        word = false;
        
      } 
      else if (Character.isLetter(input.charAt(i)) && i == endOfLine) {
        wordCount++;
        }
    }//print out the word count
    System.out.println(wordCount);
    //return value to main method
    return wordCount;
}//end numWOrds method
  //method for finding word
  public static int findText(String input){
    Scanner myScanner = new Scanner(System.in);
    //ask user for word to be found
    System.out.println("Enter a word or phrase to be found");
    //declare and initialize their input
    String word = myScanner.nextLine();
    int c = input.split(word).length - 1;
    //print out how many times it was found
    System.out.println(c);
    //return value to main method
    return c; 
  }//end method for find text
  //method for replace !
  public static String replaceExclamation(String input){
    //declare and initialize new words to replace ! with .
    String newText = input.replace('!', '.');    
    //System.out.println(newText);
    //return new text to main method 
    return newText;

  }//end replace method
  //method for shortening spaces
  public static String shortenSpace(String input){
    //declare and initialize new text to replace all spaces with >1 with just one space
    String newText = input.replaceAll("( )+", " ");
    //System.out.println(newText);
    //return the new text back to the main method
    return newText;
  }//end shorten space method
      




}//end class
//program is over!!!!!