//Daniel Reph
//September 6, 2018
//CSE 02
//This program will calculate the number of minutes for each trip, number of counts for each trip, distance of each trip, 
//and distance of two trips combined
public class Cyclometer {
  public static void main (String [] args){
  int secsTrip1 = 480;  //will count the seconds of trip 1
  int secsTrip2 = 3220; //will count the seconds of trip 2018
  int countsTrip1 = 1561; //counts in trip 1
  int countsTrip2 = 9037; //counts in trip 2
    
  double wheelDiameter = 27.0; //set wheelDiameter constant
  double PI = 3.14159; //set pi equal to constant
  double feetPerMile=5280; //set feet per mile to constant
  double inchesPerFoot=12;  //set inches per foot to constant
  double secondsPerMinute=60;  //set seconds per minute to constant
	double distanceTrip1; //set variable for total distance trip 1
  double distanceTrip2; //set variable for total distance trip 2
  double totalDistance;  //set variable for total distance for trip 1 and 2
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " +countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " +countsTrip2 + " counts.");
    
  //here we are converting all the trip measurements into miles and then calculating the total distance
  distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	totalDistance = distanceTrip1 + distanceTrip2;

    
  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");


    

    

    
  }
}
