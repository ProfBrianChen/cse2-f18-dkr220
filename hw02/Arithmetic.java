//Daniel Reph
//CSE 02
//HW 02
//September 6, 2018
public class Arithmetic{
  
    public static void main(String ars[]){
      //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per belt
      double beltCost = 33.99;
      
      //the tax rate
      double paSalesTax = 0.06;
      //calculate the total cost of pants and tax and print it
      double totalCostOfPants;
      totalCostOfPants = numPants * pantsPrice;
      System.out.println("The total cost of pants before tax is $" + totalCostOfPants);
      double taxOfPants;
      taxOfPants = paSalesTax * totalCostOfPants;
      int taxOfP = (int) (taxOfPants * 100);
      taxOfPants = taxOfP / 100.0;
      System.out.println("The tax for pants is $" + taxOfPants);
      //calculate the total cost of shirts and tax and print it
      double totalCostOfShirts;
      totalCostOfShirts = numShirts * shirtPrice;
      System.out.println("The total cost of shirts before tax is $" + totalCostOfShirts);
      double taxOfShirts;
      taxOfShirts= paSalesTax * totalCostOfShirts;
      int taxOfS = (int) (taxOfShirts * 100);
      taxOfShirts = taxOfS / 100.0;
      System.out.println("The tax for shirts is $" + taxOfShirts); 
      //calculate the total cost of belts and tax and print it
      double totalCostOfBelts;
      totalCostOfBelts = numBelts * beltCost;
      System.out.println("The total cost of belts before tax is $" + totalCostOfBelts);
      double taxOfBelts;
      taxOfBelts= paSalesTax * totalCostOfBelts;
      int taxOfB = (int) (taxOfBelts * 100);
      taxOfBelts = taxOfB / 100.0;
      System.out.println("The tax for belts is $" + taxOfBelts); 
      //calculate entire purchase cost and print it
      double purchaseCost;
      purchaseCost = totalCostOfShirts + totalCostOfPants + totalCostOfBelts;
      System.out.println("The total purchase cost before tax is $" + purchaseCost);
      //calculate total sales tax and print it
      double totalSalesTax;
      totalSalesTax = taxOfShirts + taxOfPants + taxOfBelts;
      System.out.println("The total tax is $" + totalSalesTax);
      //calculate total purchase price and print it
      double totalPaid;
      totalPaid = purchaseCost + totalSalesTax;
      System.out.println("The total paid in this transaction is $" + totalPaid);
      
      
      
     
      

  
      
    }
}
