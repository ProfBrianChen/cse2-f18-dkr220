//Daniel Reph
//CSE 002-310
//November 8, 2018
//Lab 08: Arrays
//This lab will generate a random array and count the number of occurrences of each
//number from 0 to 99 and print out the results

//import arrays and random for this lab
import java.util.Arrays;
import java.util.Random;

public class lab08{   
    public static void main(String ars[]){
      //declare and initialize the random number generator
      Random randomGenerator = new Random();      
      //System.out.println("Hi");
      //create an array for the input array
      int[] input = new int[100];
      //create an array to count the number of occurrences
      int[] counter = new int[100];
      //create a for loop to assign a random number to each spot in the array
      System.out.print("The input array holds the following integers: ");
      for(int i = 0; i < input.length; i++){
        //a number is given to each spot from 0 to 99
        input[i] = randomGenerator.nextInt(100);
        //print the value of the array at that spot
        System.out.print(input[i] + " ");
      }System.out.println();//print a new line
      //System.out.println("Input array: " + Arrays.toString(input));
      //create a for loop to go through each spot in the array
      for(int i = 0; i < input.length; i++){
        //declare and initialize a counter variable
        int occurrencesCounter = 0;
        //create a for loop to count the occurrences of each number
        for(int j = 0; j < counter.length; j++){
          //if the spot in the array is equal to the number, increase the counter
         if(input[j] == i)
           occurrencesCounter++;
          //set the index of the array equal to the counter
           counter[i] = occurrencesCounter;
        }
        //if the counter equals 1, print out this statement
        if(occurrencesCounter == 1){
          System.out.println(i +  " occurs " + occurrencesCounter + " time");
        }
        //if the counter is greater than 1, print out this statement
        else if(occurrencesCounter > 0){
          System.out.println(i +  " occurs " + occurrencesCounter + " times");
        }//don't print out anything if the counter is 0;
              
      }
      //System.out.println(Arrays.toString(counter));
    }//end main method
}//end class