//Daniel Reph
//October 25 2018
//CSE 002-310
//Lab 07: Methods
//This program will generate random sentences using methods

//import scanner and random number generator
import java.util.Random;
import java.util.Scanner;

public class Methods{   
    public static void main(String ars[]){
      //declare and initialize scanner and random number generator
      Random randomGenerator = new Random();
      Scanner myScanner = new Scanner(System.in);
      //get a random number
      int randomInt = randomGenerator.nextInt(10);
      //set up boolean
      boolean again = true;
      //run while loop while user wants to keep going
      while(again == true){
        //declare and initialize strings and call methods
        String adjectiveWord = adjectiveMethod(randomInt);
        String nonPrimaryNoun = nonPrimaryNounMethod(randomInt);
        String pastTenseVerb = pastTenseVerbMethod(randomInt);
        String nonPrimaryNoun2 = nonPrimaryNoun2Method(randomInt);
        //System.out.println(adjectiveWord);
        //System.out.println(nonPrimaryNoun);
        //System.out.println(pastTenseVerb);
        //System.out.println(nonPrimaryNoun2);
        //put together the sentence
        String finalSentence = "The " + adjectiveWord + " " + nonPrimaryNoun + " " + pastTenseVerb + " " +
          "the " + nonPrimaryNoun2 + ".";
        //print it out
        System.out.println(finalSentence);
        //ask them if they want to go again
        System.out.println("Would you like to go again, enter 1 for YES and 0 for NO");
        //take in decision
        int decision = myScanner.nextInt();
        //if 1 boolean is true, keep going
        if(decision == 1){
          again = true;
        }//if 0, boolean is false, end loop
        if(decision == 0){
          again = false;
        }
        
      }
      
        
      
      
    }//end main method
  public static String adjectiveMethod(int a){
    Random randomGenerator = new Random();
    a = randomGenerator.nextInt(10);
    String adjective = "";
    switch (a) {
      case 0: adjective = "brave";
        break;
      case 1: adjective = "calm";
        break;
      case 2: adjective = "eager";
        break;
      case 3: adjective = "gentle";
        break;
      case 4: adjective = "happy";
        break;
      case 5: adjective = "jolly";
        break;
      case 6: adjective = "kind";
        break;
      case 7: adjective = "proud";
        break;
      case 8: adjective = "silly";
        break;
      case 9: adjective = "witty";
        break;
    }//end switch statement
    return adjective;
  }//end adjectiveMethod
  public static String nonPrimaryNounMethod(int a){
    Random randomGenerator = new Random();
    a = randomGenerator.nextInt(10);
    String noun = "";
    switch (a) {
      case 0: noun = "ant";
        break;
      case 1: noun = "bee";
        break;
      case 2: noun = "cheetah";
        break;
      case 3: noun = "dog";
        break;
      case 4: noun = "eagle";
        break;
      case 5: noun = "fox";
        break;
      case 6: noun = "giraffe";
        break;
      case 7: noun = "kangaroo";
        break;
      case 8: noun = "monkey";
        break;
      case 9: noun = "snake";
        break;
    }//end switch statement
    return noun;
  }//end nonPrimaryNounMethod
  public static String pastTenseVerbMethod(int a){
    Random randomGenerator = new Random();
    a = randomGenerator.nextInt(10);
    String verb = "";
    switch (a) {
      case 0: verb = "ate";
        break;
      case 1: verb = "brought";
        break;
      case 2: verb = "called";
        break;
      case 3: verb = "dove";
        break;
      case 4: verb = "fell";
        break;
      case 5: verb = "gave";
        break;
      case 6: verb = "moved";
        break;
      case 7: verb = "ran";
        break;
      case 8: verb = "talked";
        break;
      case 9: verb = "walked";
        break;
    }//end switch statement
    return verb;
  }//end adjectiveMethod
  public static String nonPrimaryNoun2Method(int a){
    Random randomGenerator = new Random();
    a = randomGenerator.nextInt(10);
    String noun = "";
    switch (a) {
      case 0: noun = "acorn";
        break;
      case 1: noun = "building";
        break;
      case 2: noun = "car";
        break;
      case 3: noun = "doughnut";
        break;
      case 4: noun = "highway";
        break;
      case 5: noun = "motorcycle";
        break;
      case 6: noun = "snack";
        break;
      case 7: noun = "spaghetti";
        break;
      case 8: noun = "taco";
        break;
      case 9: noun = "yoyo";
        break;
    }//end switch statement
    return noun;
  }//end adjectiveMethod


}//end class