//Daniel Reph
//CSE 002-310
//October 11, 2018
//PatternD
//This program will print out different patterns

import java.util.Scanner; //import the scanner
  public class PatternD{   
    public static void main(String ars[]){ 
      //import the scanner
      Scanner myScanner = new Scanner(System.in);
      //ask the user for an integer
      System.out.println("Enter an integer between 1 and 10");
      //declare the integer as a variable
      int input = myScanner.nextInt();
      //check to make sure the number entered is valid
      while(input>10 || input < 0){
        //if not, make the user try again
        System.out.println("Invalid entry, please enter a number between 1 and 10");
        //take in new value and retest
        input = myScanner.nextInt();
      }
      int input2 = input;
      //create the loop
      for(int counter = 1; counter <= input; counter++){
        for(int counter2 = input2; counter2 > 0; counter2--){
          System.out.print(counter2 + " ");
          
        }
        input2--;
        System.out.println();
      }
    }
  }