//Daniel Reph
//CSE 02
//October 4, 2018
//UserInput
//This program will ask for class information using for loops

import java.util.Scanner; //import the scanner
  public class UserInput{   
    public static void main(String ars[]){      
      Scanner myScanner = new Scanner(System.in);
      //set initial boolean values to false so while loop can run
      boolean correct = false; 
      boolean correct2 = false;
      boolean correct3 = false;
      boolean correct4 = false;
      boolean correct5 = false;
      boolean correct6 = false;
      //set variables for each category whether they should be strings or integers
      int x = 0;
      String y = "";
      int z = 0;
      int w = 0;
      String v = "";
      int u = 0;
      //first while loop, begin with false
      while(correct == false){
        //ask user for course number
        System.out.println("Enter the course number");
        //see whether what user entered is integer or not
        correct = myScanner.hasNextInt();
        //if it is an integer
        if(correct){
          //assign integer value to variable
          x = myScanner.nextInt();
        }
        //if not an integer, tell user it's invalid and to try again
        else{
          System.out.println("Invalid entry, need an integer.");
          //remove their entry
          myScanner.next();
          //make boolean false so while loop can run again
          correct = false;
        }
      }
      //begin with false to enter while loop      
      while(correct2== false){
        //ask user for name
        System.out.println("Enter the department name");
        //check to see if input is NOT integer OR double
        if(myScanner.hasNextInt() || myScanner.hasNextDouble()){
          //if it is integer or double make boolean false again
          correct2 = false;
          //tell user entry is invalid, try again
          System.out.println("Invalid entry, need a string");
          //remove entry from scanenr
          myScanner.next();
        }
        else{
          //if true set variable to scanner value
          y = myScanner.next();
          //make boolean true to exit while loop
          correct2 = true;
        }
      }
      //same thing here again with integer
      while(correct3 == false){
        System.out.println("Enter the number of times it meets a week");
        correct3 = myScanner.hasNextInt();
        if(correct3){
          z = myScanner.nextInt();
        }
        else{
          System.out.println("Invalid entry, need an integer.");
          myScanner.next();
          correct3 = false;
        }
      }
      //same thing here again with string
      while(correct4== false){
        System.out.println("Enter the time the class meets in the form XX:XX");
        //here we ask for a specific form in a string value
        //if the user enters 2 for 2 o'clock, the boolean will give a false value
        //they must enter it in the form XX:XX, like 11:20 for example
        if(myScanner.hasNextInt() || myScanner.hasNextDouble()){
          correct4 = false;
          System.out.println("Invalid entry, need a string");
          myScanner.next();
        }
        else{
          y = myScanner.next();
          correct4 = true;
        }
      }
      while(correct5== false){
        System.out.println("Enter the instructor name");
        if(myScanner.hasNextInt() || myScanner.hasNextDouble()){
          correct5 = false;
          System.out.println("Invalid entry, need an integer");
          myScanner.next();
        }
        else{
          y = myScanner.next();
          correct5 = true;
        }
      }
      while(correct6 == false){
        System.out.println("Enter the number of students");
        correct6 = myScanner.hasNextInt();
        if(correct6){
          x = myScanner.nextInt();
        }
        else{
          System.out.println("Invalid entry, need an integer.");
          myScanner.next();
          correct6 = false;
        }
      }
    }
  }
      
     
      
     