//Daniel Reph
//CSE 02 CardGenerator
//September 20, 2018
//This program is going to generate a random card
//from a deck of 52
  public class CardGenerator{   
    public static void main(String ars[]){
      //First, generate a random number for the card
      int card = (int)(Math.random()*52)+1;
      int spadeCard;
      int clubCard;
      int heartCard;
      int diamondCard;
      //make two string variables
      String cardSuit;
      String cardIdentity = "";
      //if number is abover 39, you have spade      
      if(card>39){
        cardSuit = "spades";
        //get remainder of card
        spadeCard = card%13;
        //swith number to card identity
        switch (spadeCard) {
          case 0:  cardIdentity = "Ace";
            break;
          case 1:  cardIdentity = "Two";
            break;
          case 2: cardIdentity = "Three";
            break;      
          case 3:  cardIdentity = "Four";
            break;
          case 4:  cardIdentity = "Five";
            break;
          case 5:  cardIdentity = "Six";
            break;
          case 6:  cardIdentity = "Seven";
            break;
          case 7:  cardIdentity = "Eight";
            break;
          case 8:  cardIdentity = "Nine";
            break;
          case 9:  cardIdentity = "Ten";
            break;
          case 10: cardIdentity = "Jack";
            break;
          case 11: cardIdentity = "Queen";
            break;
          case 12: cardIdentity = "King";
            break;
           
         
        }
        //print out the card you got
        System.out.println("You picked the " +cardIdentity +" of " +cardSuit);
      }
      //if card is between 26 and 39 you got clubs
           if(card>26&&card<39){
        cardSuit = "clubs";
             //get remainder
        clubCard = card%13;
             //switch remainder to identity
        switch (clubCard) {
          case 0:  cardIdentity = "Ace";
            break;
          case 1:  cardIdentity = "Two";
            break;
          case 2: cardIdentity = "Three";
            break;      
          case 3:  cardIdentity = "Four";
            break;
          case 4:  cardIdentity = "Five";
            break;
          case 5:  cardIdentity = "Six";
            break;
          case 6:  cardIdentity = "Seven";
            break;
          case 7:  cardIdentity = "Eight";
            break;
          case 8:  cardIdentity = "Nine";
            break;
          case 9:  cardIdentity = "Ten";
            break;
          case 10: cardIdentity = "Jack";
            break;
          case 11: cardIdentity = "Queen";
            break;
          case 12: cardIdentity = "King";
            break;
           
         
        } //print what card you got
           System.out.println("You picked the " +cardIdentity +" of " +cardSuit);

      }
      //if between 13 and 26 you got hearts
      if(card>13&&card<26){
        cardSuit = "hearts";
        //get remainder
        heartCard = card%13;
        //switch remainder to identity
        switch (heartCard) {
          case 0:  cardIdentity = "Ace";
            break;
          case 1:  cardIdentity = "Two";
            break;
          case 2: cardIdentity = "Three";
            break;      
          case 3:  cardIdentity = "Four";
            break;
          case 4:  cardIdentity = "Five";
            break;
          case 5:  cardIdentity = "Six";
            break;
          case 6:  cardIdentity = "Seven";
            break;
          case 7:  cardIdentity = "Eight";
            break;
          case 8:  cardIdentity = "Nine";
            break;
          case 9:  cardIdentity = "Ten";
            break;
          case 10: cardIdentity = "Jack";
            break;
          case 11: cardIdentity = "Queen";
            break;
          case 12: cardIdentity = "King";
            break;
           
         
        }  //print what card you got
          System.out.println("You picked the " +cardIdentity +" of " +cardSuit);

      }
      //if between 0 and 13 you got diamonds
      if(card>0&&card<13){
        cardSuit = "diamonds";
        //get remainder
        diamondCard = card%13;
         //switch to identity
        switch (diamondCard) {
          case 0:  cardIdentity = "Ace";
            break;
          case 1:  cardIdentity = "Two";
            break;
          case 2: cardIdentity = "Three";
            break;      
          case 3:  cardIdentity = "Four";
            break;
          case 4:  cardIdentity = "Five";
            break;
          case 5:  cardIdentity = "Six";
            break;
          case 6:  cardIdentity = "Seven";
            break;
          case 7:  cardIdentity = "Eight";
            break;
          case 8:  cardIdentity = "Nine";
            break;
          case 9:  cardIdentity = "Ten";
            break;
          case 10: cardIdentity = "Jack";
            break;
          case 11: cardIdentity = "Queen";
            break;
          case 12: cardIdentity = "King";
            break;
           
         
        }  //print out final statement
           System.out.println("You picked the " +cardIdentity +" of " +cardSuit);

      }
      
        
        
 }
}