//Daniel Reph
//CSE 02
//October 4, 2018
//HW 05
//The objective of this program is to ask the user how many poker hands
//they would like to generate and then generate random cards and see
//if they match with any one of the four poker hands in this case

//first import the scanner
import java.util.Scanner;

public class hw05{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    //begin a counter for each of the hands we are tracking
    int onePairCounter = 0;
    int twoPairCounter = 0;
    int threeOfAKindCounter = 0;
    int fourOfAKindCounter = 0;
    //create a boolean for checking to make sure the user enters an integer
    boolean correct = false;
    //initialize the number of hands to be zero
    int numHands = 0;
    //begin a while loope to make sure the user enters an integer
    while(correct == false){
        //ask user for number of hands
        System.out.println("Enter the number of hands you would like to generate");
        //see whether what user entered is integer or not
        correct = myScanner.hasNextInt();
        //if it is an integer
        if(correct){
          //assign integer value to variable
         numHands = myScanner.nextInt();
        }
      //if not an integer, tell user it's invalid and to try again
        else{
          System.out.println("Invalid entry, need an integer.");
          //remove their entry
          myScanner.next();
          //make boolean false so while loop can run again
          correct = false;
        }
      }
    //begin a for loop to generate a set of cards for each hand
    for(int i = 1; i <= numHands; i++){
      // here we generate the first two cards randomly
      int card1 = (int)(Math.random()*(52))+1;
      
      int card2 = (int)(Math.random()*(52))+1;
      //here we check to see if the cards are the same number
      //if they are we assign a new value to card 2 and try again 
      while (card2 == card1){
        card2 = (int)(Math.random()*(52))+1;
      }// here we do the same thing for card 3, checking against card 1 and card 2
      int card3 = (int)(Math.random()*(52))+1;
      while (card3 == card2 || card3 == card1){
        card3 = (int)(Math.random()*(52))+1;
      }// same thing for card 4
      int card4 = (int)(Math.random()*(52))+1;
      while (card4 == card2 || card4 == card1 || card4 == card3){
        card4 = (int)(Math.random()*(52))+1;
      }//repeated again for card 5
      int card5 = (int)(Math.random()*(52))+1;
      while (card5 == card2 || card5 == card1 || card5 == card3 || card5 == card4){
        card5 = (int)(Math.random()*(52))+1;
      }
      //here we are taking each card and defining it by a number
      //whatever the remainder is is going to be the rank of the card
      //i.e. 1 is an ace, 0 is a king, not that it really matters
      card1 = card1 % 13;
      card2 = card2 % 13;
      card3 = card3 % 13;
      card4 = card4 % 13;
      card5 = card5 % 13;
      //here we check all the possibilities for a four of a kind to occur
      if (card1 == card2 && card1 == card3 && card1 == card4 ||
         card1 == card2 && card1 == card3 && card1 == card5 ||
         card1 == card2 && card1 == card4 && card1 == card5 ||
         card1 == card3 && card1 == card4 && card1 == card5||
         card2 == card3 && card2 == card4 && card3 == card5){
        //if one of the conditions is met, we increment the counter by 1
        fourOfAKindCounter++;
      }//here we check the possbilities for three of a kind
      if (card1 == card2 && card1 == card3 || card1 == card2 && card1 == card4 ||
         card1 == card2 && card1 == card5 || card1 == card3 && card1 == card4 ||
         card1 == card3 && card1 == card5 || card1 == card4 && card1 == card5 ||
         card2 == card3 && card3 == card4 || card2 == card3 && card2 == card5 ||
         card2 == card4 && card2 == card5 || card3 == card4 && card3 == card5){
        //if we have one, we increment counter by 1
        threeOfAKindCounter++;
      }//check for two pair
      if (card1 == card2 && card3 == card4 || card1 == card2 && card3 == card5 ||
         card1 == card2 && card4 == card5 || card1 == card3 && card2 == card5 ||
          card1 == card3 && card2 == card5 || card1 == card3 && card2 == card4 ||
          card1 == card3 && card4 == card5 || card1 == card4 && card2 == card5 ||
          card1 == card4 && card2 == card3 || card1 == card4 && card3 == card5 ||
          card1 == card5 && card2 == card4 || card1 == card5 && card2 == card3 ||
          card1 == card5 && card3 == card4 || card2 == card3 && card4 == card5 ||
          card2 == card4 && card3 == card5 || card2 == card5 && card3 == card4){
        //increment counter if so
        twoPairCounter++;
      }
      //check for one pair  
      if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 ||
         card2 == card3 || card2 == card4 || card2 == card5 || card3 == card4 ||
         card4 == card5){
        //increment counter once again if true
        onePairCounter++;
      }
      
    }
    System.out.println(onePairCounter);
    System.out.println(twoPairCounter);
    System.out.println(threeOfAKindCounter);
    System.out.println(fourOfAKindCounter);
    
    //here we are going to compute the probabilities of each hand
    //we will declare and initialize each probability
    //when dividing, we have to cast each counter and numHands as doubles so we 
    //are returned with a decimal value
    double onePairProbability = (double)onePairCounter / (double)numHands;
    double twoPairProbability = (double)twoPairCounter / (double)numHands;
    double threeOfAKindProbability = (double)threeOfAKindCounter / (double)numHands;
    double fourOfAKindProbability = (double)fourOfAKindCounter / (double)numHands;
    
    //finally we print out how many loops we ran through, which equals the number of hands requested
    //then we print out each probability, making sure to format it with a printf statement
    //and giving each output three decimal places
    System.out.println("The number of loops: " + numHands);
    System.out.printf("The probability of a one pair is: %.3f \n", onePairProbability);
    System.out.printf("The probability of a two pair is: %.3f \n", twoPairProbability);
    System.out.printf("The probability of a three of a kind is: %.3f \n", threeOfAKindProbability);
    System.out.printf("The probability of a four of a kind is: %.3f \n", fourOfAKindProbability);
    
    
    
    
    
  }
  
}
