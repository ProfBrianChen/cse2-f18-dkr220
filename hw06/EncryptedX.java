//Daniel Reph
//CSE 002-310
//October 19, 2018
//HW 06: EncryptedX
//This program will ask the user for an integer between 1 and 100
//and make a pattern with asterisks showing an X, varying in size

//first import the scanner
import java.util.Scanner;
public class EncryptedX{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    boolean correct = false;
    int size = 0;
    while(correct == false){
        //ask user for number between 1 and 100
        System.out.println("Enter a number between 1 and 100");
        //see whether what user entered is integer or not
        correct = myScanner.hasNextInt();
        //if it is an integer
        if(correct){
          //assign integer value to variable
         size = myScanner.nextInt();
          //check to see if it's in range of 1 and 100
          if(size <=100 && size >=1){
            //if it is, keep it as variable, you're good to go
            size = size;
           }
          //if it's not, make them choose again
          else{
            //tell them it's invalid
            System.out.println("Invalid");
            //make boolean false to run loop again
            correct = false;
          }
        }
      //if not an integer, tell user it's invalid and to try again
        else{
          System.out.println("Invalid. Please enter an integer between 1 and 100.");
          //remove their entry
          myScanner.next();
          //make boolean false so while loop can run again
          correct = false;
        }
      }
    //begin for loop, initialize row as zero, check to see
    //if row is less than size of X + 1, and increment row by 1
    for (int row = 0; row < size + 1; row++){
      //begin for loop, initialize column as zero, check to see
      //if columns is less than X + 1, and increment column by 1
      for (int column = 0; column < size + 1; column++){
        //here we check to see if the row = column or row + column = size of X
        //if it does, we should put space in that place, forming the X
        if (row == column || row + column == size){
          System.out.print(" ");
        }
        //if not, we put an asterisk in the space
        else{
          System.out.print("*");
        }//end else
      }//end second for
      //print a new line when the final column is finished running
      System.out.println();
    }//end first for
      
  }//end of main
}//end of class
               
                        
                          
