//Daniel Reph
//CSE 02 CrapsIf
//September 21, 2018
//This program is going to give you the name of 
//the two die combination that you roll
import java.util.Scanner; //import the scanner
import java.util.Random; //import the random number generator
  public class CrapsIf{   
    public static void main(String ars[]){
      //ask the user if they want random or pick their own
      System.out.println("Enter 1 if you would like to randomly generate dice, 2 if you want input dice");
      Scanner myScanner = new Scanner(System.in);
      Random rand = new Random();
      //take the user's choice
      int choice = myScanner.nextInt();
      //make sure they gave valid choice
      if (choice !=2 && choice !=1){
        System.out.println("Invalid Choice");
      }
      //go through if they want random
      if (choice == 1){
        //give dice values 1 to 6
        int dice1 = rand.nextInt(6) + 1;
        int dice2 = rand.nextInt(6) + 1;
        //print out what numbers were chosen
        System.out.println(dice1+ " " + dice2);
        //if you get two 1's
        if (dice1 == 1 && dice2 == 1){
          //print out the name of the combo
          System.out.println("Snake Eyes");
        }//same thing gets repeated for the rest
        if (dice1 == 1 && dice2 == 2 || dice1 == 2 && dice2 == 1){
          System.out.println("Ace Deuce");
        }
        if(dice1 == 1 && dice2 == 3 || dice1 == 3 && dice2 == 1){
          System.out.println("Easy Four");
        }
        if(dice1 == 1 && dice2 == 4|| dice1 == 4 && dice2 == 1){
          System.out.println("Fever Five");
        }
        if(dice1 == 1 && dice2 == 5 || dice1 == 5 && dice2 == 1){
          System.out.println("Easy Six");
        }
        if(dice1 == 1 && dice2 == 6 || dice1 == 6 && dice2 == 1){
          System.out.println("Seven Out");
        }
        if(dice1 == 2 && dice2 == 2){
          System.out.println("Hard Four");
        }
        if(dice1 == 2 && dice2 == 3 || dice1 == 3 && dice2 == 2){
          System.out.println("Fever Five");
        }
        if(dice1 == 2 && dice2 == 4 || dice1 == 4 && dice2 == 2){
          System.out.println("Easy Six");
        }
        if(dice1 == 2 && dice2 == 5 || dice1 == 5 && dice2 == 2){
          System.out.println("Seven Out");
        }
        if(dice1 == 2 && dice2 == 6 || dice1 == 6 && dice2 == 2){
          System.out.println("Easy Eight");
        }
        if(dice1 == 3 && dice2 == 3){
          System.out.println("Hard Six");
        }
        if(dice1 == 3 && dice2 == 4 || dice1 == 4 && dice2 == 3){
          System.out.println("Seven Out");
        }
        if(dice1 == 3 && dice2 == 5 || dice1 == 5 && dice2 == 3){
          System.out.println("Easy Eight");
        }
        if(dice1 == 3 && dice2 == 6 || dice1 == 6 && dice2 == 3){
          System.out.println("Nine");
        }
        if(dice1 == 4 && dice2 == 4){
          System.out.println("Hard Eight");
        }
        if(dice1 == 4 && dice2 == 5 || dice1 == 5 && dice2 == 4){
          System.out.println("Nine");
        }
        if(dice1 == 4 && dice2 == 6 || dice1 == 6 && dice2 == 4){
          System.out.println("Easy Ten");
        }
        if(dice1 == 5 && dice2 == 5){
          System.out.println("Hard Ten");
        }
        if(dice1 == 5 && dice2 == 6 || dice1 == 6 && dice2 == 5){
          System.out.println("Yo-leven");
        }
        if(dice1 == 6 && dice2 == 6){
          System.out.println("Hard Eight");
      }}//if you choose to pick your own number
      if (choice == 2){
        //ask them for one number
        System.out.println("Pick a number between 1 and 6");
        //assign that number to a variable
        int dice1 = myScanner.nextInt();
        //ask them for another number
        System.out.println("Pick a number between 1 and 6");
        //assign that number to another variable
        int dice2 = myScanner.nextInt();
        if (dice1<0 || dice1>6 || dice2<0 || dice2>6){
          //make sure the dice numbers are valid
          System.out.println("Invalid dice numbers");
        }
        //if dice are both ones
        if (dice1 == 1 && dice2 == 1){
          //print out combo name
          System.out.println("Snake Eyes");
        }//and so on and so forth...
        if (dice1 == 1 && dice2 == 2 || dice1 == 2 && dice2 == 1){
          System.out.println("Ace Deuce");
        }
        if(dice1 == 1 && dice2 == 3 || dice1 == 3 && dice2 == 1){
          System.out.println("Easy Four");
        }
        if(dice1 == 1 && dice2 == 4|| dice1 == 4 && dice2 == 1){
          System.out.println("Fever Five");
        }
        if(dice1 == 1 && dice2 == 5 || dice1 == 5 && dice2 == 1){
          System.out.println("Easy Six");
        }
        if(dice1 == 1 && dice2 == 6 || dice1 == 6 && dice2 == 1){
          System.out.println("Seven Out");
        }
        if(dice1 == 2 && dice2 == 2){
          System.out.println("Hard Four");
        }
        if(dice1 == 2 && dice2 == 3 || dice1 == 3 && dice2 == 2){
          System.out.println("Fever Five");
        }
        if(dice1 == 2 && dice2 == 4 || dice1 == 4 && dice2 == 2){
          System.out.println("Easy Six");
        }
        if(dice1 == 2 && dice2 == 5 || dice1 == 5 && dice2 == 2){
          System.out.println("Seven Out");
        }
        if(dice1 == 2 && dice2 == 6 || dice1 == 6 && dice2 == 2){
          System.out.println("Easy Eight");
        }
        if(dice1 == 3 && dice2 == 3){
          System.out.println("Hard Six");
        }
        if(dice1 == 3 && dice2 == 4 || dice1 == 4 && dice2 == 3){
          System.out.println("Seven Out");
        }
        if(dice1 == 3 && dice2 == 5 || dice1 == 5 && dice2 == 3){
          System.out.println("Easy Eight");
        }
        if(dice1 == 3 && dice2 == 6 || dice1 == 6 && dice2 == 3){
          System.out.println("Nine");
        }
        if(dice1 == 4 && dice2 == 4){
          System.out.println("Hard Eight");
        }
        if(dice1 == 4 && dice2 == 5 || dice1 == 5 && dice2 == 4){
          System.out.println("Nine");
        }
        if(dice1 == 4 && dice2 == 6 || dice1 == 6 && dice2 == 4){
          System.out.println("Easy Ten");
        }
        if(dice1 == 5 && dice2 == 5){
          System.out.println("Hard Ten");
        }
        if(dice1 == 5 && dice2 == 6 || dice1 == 6 && dice2 == 5){
          System.out.println("Yo-leven");
        }
        if(dice1 == 6 && dice2 == 6){
          System.out.println("Hard Eight");
        }
      }
    }
  }