//Daniel Reph
//CSE 02 CrapsSwitch
//September 21, 2018
//This program is going to give you the name of 
//the two die combination that you roll
import java.util.Scanner; //import the scanner
import java.util.Random; //import the random number generator
  public class CrapsSwitch{   
    public static void main(String ars[]){
      //as the user if they want random numbers or their own
      System.out.println("Enter 1 if you would like to randomly generate dice, 2 if you want input dice");
      Scanner myScanner = new Scanner(System.in);
      Random rand = new Random();
      //record their choice and assign it
      int choice = myScanner.nextInt();
      //check to make sure their choice is valid
      if (choice !=1 && choice != 2){
        //print statement if invalid
        System.out.println("Invalid choice.");
        return;
      }
      //if the want random use this if statement
      if (choice == 1){
        //get two dice number
        int dice1 = rand.nextInt(6) + 1;
        int dice2 = rand.nextInt(6) + 1;
        //print out what numbers were chosen
        System.out.println(dice1+ " " + dice2);
        
        //use switch statement for each case
        //based on dice1 initially
        switch (dice1){
          case 1: 
            switch(dice2){
                //now we print statements depending on dice2 number
              case 1: System.out.println("Snake Eyes");
                break;
              case 2: System.out.println("Ace Deuce");
                break;
              case 3: System.out.println("Easy Four");
                break;
              case 4: System.out.println("Fever Five");
                break;
              case 5: System.out.println("Easy Six");
                break;
              case 6: System.out.println("Seven Out");
                break;
            }
            break;
            //now is for dice1 being 2
          case 2: 
            switch(dice2){
                //and so on and so forth...
                case 1: System.out.println("Ace Deuce");
                break;
                case 2: System.out.println("Hard Four");
                break;
              case 3:System.out.println("Fever Five");
                break;
              case 4:System.out.println("Easy Six");
                break;
              case 5:System.out.println("Seven Out");
                break;
              case 6:System.out.println("Easy Eight");
                break;
            }
            break;
          case 3:
            switch(dice2){
              case 1: System.out.println("Easy Four");
              break;
              case 2: System.out.println("Fever Five");
              break;
              case 3: System.out.println("Hard Six");
              break;
              case 4: System.out.println("Seven Out");
              break;
              case 5: System.out.println("Easy Eight");
              break;
              case 6: System.out.println("Nine");
              break;
              
            }
            break;
          case 4:
            switch(dice2){
              case 1: System.out.println("Fever Five");
              break;
              case 2: System.out.println("Easy Six");
              break;
              case 3: System.out.println("Seven Out");
              break;
              case 4: System.out.println("Hard Eight");
              break;
              case 5: System.out.println("Nine");
              break;
              case 6: System.out.println("Easy Ten");
              break;
              
            }
            break;
          case 5:
            switch(dice2){
              case 1: System.out.println("Easy Six");
              break;
              case 2: System.out.println("Seven Out");
              break;
              case 3: System.out.println("Easy Eight");
              break;
              case 4: System.out.println("Nine");
              break;
              case 5: System.out.println("Hard Ten");
              break;
              case 6: System.out.println("Yo-leven");
              break;
              
            }
            break;
          case 6:
            switch(dice2){
              case 1: System.out.println("Seven Out");
              break;
              case 2: System.out.println("Easy Eight");
              break;
              case 3: System.out.println("Nine");
              break;
              case 4: System.out.println("Easy Ten");
              break;
              case 5: System.out.println("Yo-leven");
              break;
              case 6: System.out.println("Boxcars");
              break;
              
            }
            break;
            
        }
    }
      //now we use this if they want their own numbers
      else if(choice == 2){
        //ask them to enter the first number
        System.out.println("Please enter a digit between 1 and 6");
        int dice1 = myScanner.nextInt();
        //ask them to enter the second number
        System.out.println("Please enter a digit between 1 and 6");
        int dice2 = myScanner.nextInt();
        //check to make sure numbers are valid
        if (dice1>6 || dice1<0 || dice2>6 || dice2<0){
          System.out.println("Input is not valid");
          return;
        } 
        
        //start switch here
        //same thing as before
        switch (dice1){
          case 1: 
            switch(dice2){
              case 1: System.out.println("Snake Eyes");
                break;
              case 2: System.out.println("Ace Deuce");
                break;
              case 3: System.out.println("Easy Four");
                break;
              case 4: System.out.println("Fever Five");
                break;
              case 5: System.out.println("Easy Six");
                break;
              case 6: System.out.println("Seven Out");
                break;
            }
            break;
            
          case 2: 
            switch(dice2){
                case 1: System.out.println("Ace Deuce");
                break;
                case 2: System.out.println("Hard Four");
                break;
              case 3:System.out.println("Fever Five");
                break;
              case 4:System.out.println("Easy Six");
                break;
              case 5:System.out.println("Seven Out");
                break;
              case 6:System.out.println("Easy Eight");
                break;
            }
            break;
          case 3:
            switch(dice2){
              case 1: System.out.println("Easy Four");
              break;
              case 2: System.out.println("Fever Five");
              break;
              case 3: System.out.println("Hard Six");
              break;
              case 4: System.out.println("Seven Out");
              break;
              case 5: System.out.println("Easy Eight");
              break;
              case 6: System.out.println("Nine");
              break;
              
            }
            break;
          case 4:
            switch(dice2){
              case 1: System.out.println("Fever Five");
              break;
              case 2: System.out.println("Easy Six");
              break;
              case 3: System.out.println("Seven Out");
              break;
              case 4: System.out.println("Hard Eight");
              break;
              case 5: System.out.println("Nine");
              break;
              case 6: System.out.println("Easy Ten");
              break;
              
            }
            break;
          case 5:
            switch(dice2){
              case 1: System.out.println("Easy Six");
              break;
              case 2: System.out.println("Seven Out");
              break;
              case 3: System.out.println("Easy Eight");
              break;
              case 4: System.out.println("Nine");
              break;
              case 5: System.out.println("Hard Ten");
              break;
              case 6: System.out.println("Yo-leven");
              break;
              
            }
            break;
          case 6:
            switch(dice2){
              case 1: System.out.println("Seven Out");
              break;
              case 2: System.out.println("Easy Eight");
              break;
              case 3: System.out.println("Nine");
              break;
              case 4: System.out.println("Easy Ten");
              break;
              case 5: System.out.println("Yo-leven");
              break;
              case 6: System.out.println("Boxcars");
              break;
              
            }
            break;
            
        }
      }
  }
  }