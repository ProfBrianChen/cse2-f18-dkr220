//Daniel Reph
//CSE 002-310
//November 15, 2018
//Lab 09: Passing arrays inside methods
//This program will pass arrays to methods and play around with them
//import arrays package
import java.util.Arrays;
public class lab09 {
    public static void main(String args[]) {
      //create an array
      int[] array0 = {0, 2, 4, 6, 8, 10, 12, 14};
      //System.out.println(Arrays.toString(array0));
      //create array1, copying array0
      int[] array1 = copy(array0);
      //create array2, copying array1;
      int[] array2 = copy(array0);
      //printing out all arrays, making sure they're the same
      System.out.println("Array0: " + Arrays.toString(array0));
      System.out.println("Array1: " + Arrays.toString(array1));
      System.out.println("Array2: " + Arrays.toString(array2));
      //invert the array0
      inverter(array0);
      //System.out.println("Inverted array0");
      //print(array0);
      //invert the array1 using other method
      inverter2(array1);
      //print(array1);
      //make array3, invert array2
      int[] array3 = inverter2(array2);
      //print(array3);
      //System.out.println("Arrays 0, 1, 2, 3:");
      //print out what each array is after methods
      System.out.println();
      System.out.print("Array0: ");
      print(array0);
      System.out.print("Array1: ");
      print(array1);
      //System.out.print("Array 2: ");
      //print(array2);
      System.out.print("Array3: ");
      print(array3);
    }//end main
  //make a copy method
  public static int[] copy(int myList1[]){
    //make new list, same length as input list
    int[] newList = new int[myList1.length];
    //use for loop to copy each value
    for(int i = 0; i < myList1.length;i++){
      //set the new arrays equal to each other
      newList[i] = myList1[i];
    }//return copied list
    return newList;
  }//end copy method
  //make inverter method
  public static void inverter(int inputList[]){
    //create temp list
    int[] tempList = new int[inputList.length];
    //tell temp list to be a copy of input list
    tempList = copy(inputList);
    //System.out.println(Arrays.toString(inputList));
    //System.out.println(Arrays.toString(tempList));
    //create for loop to switch values in reverse
    for(int i = 0; i < inputList.length; i++){
      inputList[i] = tempList[inputList.length - i - 1];
      //System.out.print(inputList[i] + " ");
    }
    //set them equal
    inputList = tempList;
  }//end inverter method
  //inverter2 method
  public static int[] inverter2(int inputList[]){
    //make new array
    int[] copyArray = new int[inputList.length];
    //use copy method to copy input
    copyArray = copy(inputList);
    int[] tempList = new int[inputList.length];
    //use for loop to invert array
    for(int i = 0; i < inputList.length; i++){
      copyArray[i] = inputList[tempList.length - i - 1];
      //System.out.print(copyArray[i] + " ");
        }
    //return new array
    return copyArray;
  }//end inverter2
  //print method
  public static void print(int inputList[]){
    //create for loop to print each value in array
    for(int i = 0; i < inputList.length; i++){
      System.out.print(inputList[i]+ " ");
    }//print new line after for loop
    System.out.println();
  }//end print method
}//end class
