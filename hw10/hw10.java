//Daniel Reph
//CSE 002-310
//December 1, 2018
//HW 10
//This program will simulate a tic tac toe game
//import helpers
import java.util.Arrays;
import java.util.Scanner;
public class hw10{
  public static void main(String args[]){
    //initialize scanner
    Scanner myScanner = new Scanner(System.in);
    //declare MD array
    int[] usedNumbers = new int[9];
    //make the board
    char[][] board = new char[3][3];
    //set initial value
    char k = 49;
    //use double nested for loop to make board
    for(int i = 0; i < 3; i++){
      for(int j = 0; j < 3; j++){
        board[i][j] = k;
        k++;
      }  
    }
    //start counter to keep track of turns
    int counter = 0;
    //make boolean to keep track of if there is a winner
    boolean win = false;
    //set counter up to 9 turns
    while(counter < 9){ 
      //print out the board for the user
      print(board);
      //declaer initial entry number
      int entryNumber = 1;
      //make boolean to keep track of good entry
      boolean answer = false;
      while(answer == false){
        //check to see whose turn it is
        if(counter%2 == 0){
          //tell O to go
        System.out.println("O's turn: pick the spot where you would like to go");
      }
        else{    
          //tell X to go
          System.out.println("X's turn: pick the spot where you would like to go");
        }//check for good entry, can't be non-int
        boolean loop = false;
        while(loop == false){    
          //make boolean
            boolean checkInt = myScanner.hasNextInt();

        if(checkInt == true){
          //if entry is good, make it the entry number
            entryNumber = myScanner.nextInt();
            //System.out.println(Arrays.toString(usedNumbers));
            for(int a = 0; a < 9; a++){
              //check for used numbers
                if(usedNumbers[a] == entryNumber){
                  //if chosen a bad space, tell them to try again
                    System.out.println("Space occupied, go again");
                  counter--;
                    break;
                }
            } 
          //put the entry into an array of used numbers
          usedNumbers[counter] = entryNumber;
          //tell them what their entry was
          System.out.println("Your entry was: " + entryNumber);
          loop = true;
        }//if not good
          else if(checkInt == false){
            //tell them to enter an int
            System.out.println("Invalid, enter an integer");
            loop = false;
            myScanner.nextLine();
          }
        }
        //try again
        answer = check(entryNumber);
      }
      //go to method to entry the value
      entryMethod(board, counter, entryNumber);
      //go to method to see if somebody won
      win = checkWin(board);
      //if win, break while loop
      if(win == true){
        break;
      }
      //increase counter
      counter++;
    }//end while loop
    //if reaches end of counter and nobody won, tell them
    if(win == false){
    System.out.println("Nobody won");
    }
    
  }//end main
  public static void print(char[][] board){
    for(int row = 0; row < board.length; row++){
      for(int column = 0; column < board[row].length; column++){
        //print out the board when method is called
        System.out.print(board[row][column] + " ");
      }
      System.out.println();
    }
  }//check entry is good
  public static boolean check(int entryNumber){
    //make sure within range and not a double
    if(entryNumber > 0 && entryNumber < 10 && entryNumber%1 == 0){
      return true;
    }
    else{
      //tell them to go again if failed
      System.out.println("Invalid, go again");
      return false;
    }
    
  }//put the entry number into the board as either x or o
  public static char[][] entryMethod(char[][] board, int counter, int entryNumber){
    if(counter%2 == 1){
        char entryChar = 'X';
        if(entryNumber == 1){
            board[0][0] = entryChar;
        }
        if(entryNumber == 2){
            board[0][1] = entryChar;
        }
        if(entryNumber == 3){
            board[0][2] = entryChar;
        }
        if(entryNumber == 4){
            board[1][0] = entryChar;
        }
        if(entryNumber == 5){
            board[1][1] = entryChar;
        }
        if(entryNumber == 6){
            board[1][2] = entryChar;
        }
        if(entryNumber == 7){
            board[2][0] = entryChar;
        }
        if(entryNumber == 8){
            board[2][1] = entryChar;
        }
        if(entryNumber == 9){
            board[2][2] = entryChar;
        }
    }
        else if(counter%2 == 0){
            char entryChar= 'O';
            if(entryNumber == 1){
            board[0][0] = entryChar;
        }
        if(entryNumber == 2){
            board[0][1] = entryChar;
        }
        if(entryNumber == 3){
            board[0][2] = entryChar;
        }
        if(entryNumber == 4){
            board[1][0] = entryChar;
        }
        if(entryNumber == 5){
            board[1][1] = entryChar;
        }
        if(entryNumber == 6){
            board[1][2] = entryChar;
        }
        if(entryNumber == 7){
            board[2][0] = entryChar;
        }
        if(entryNumber == 8){
            board[2][1] = entryChar;
        }
        if(entryNumber == 9){
            board[2][2] = entryChar;
        }
        }
        
    return board;
  }//end entryMethod
  //check to see if somebody won
  public static boolean checkWin(char[][] board){
    //check multiple conditions
    //make boolean false, return true if condition met
    boolean answer = false;
    if(board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X' ||
      board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X' ||
      board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X' ||
      board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X' ||
      board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X' ||
      board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X' ||
      board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X' ||
      board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X'){
      //print the board
      print(board);
      //tell them who won
      System.out.println("X wins, congratulations");
      answer = true;
    }//same thing for O
    else if(board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O' ||
      board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O' ||
      board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O' ||
      board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O' ||
      board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O' ||
      board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O' ||
      board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O' ||
      board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O'){
      System.out.println("O wins, congratulations");
      print(board);
      answer = true;
    }
    return answer;
  }//end checkWin method
}//end class
