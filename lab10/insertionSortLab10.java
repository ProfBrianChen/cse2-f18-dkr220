//Daniel Reph
//CSE 002-310
//December 6, 2018
//Lab 10: Sorting
//This program will sort arrays and count the number of steps it takes
import java.util.Arrays;
public class insertionSortLab10{
  public static void main(String[] args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst =  {9,8,7,6,5,4,3,2,1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    
  }//end main
  public static int insertionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iterations = 0;
    for(int i = 1; i < list.length; i++){
      iterations++;
      for(int j = i; j > 0; j--){
        if(list[j] < list[j-1]){
          iterations++;
          int temp = list[j];
          list[j] = list[j-1];
          list[j-1] = temp;
          //System.out.println(Arrays.toString(list));
        }
        else{
          break;
        }
        //System.out.println(Arrays.toString(list));
      }
      System.out.println(Arrays.toString(list));
  
    }
    return iterations;
  }
}//end class