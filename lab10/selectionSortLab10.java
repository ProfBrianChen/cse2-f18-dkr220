//Daniel Reph
//CSE 002-310
//December 6, 2018
//Lab 10: Sorting
//This program will sort arrays and count the number of steps it takes
import java.util.Arrays;
public class selectionSortLab10{
  public static void main(String[] args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = selectionSort(myArrayBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
  }//end main
  public static int selectionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iterations = 0;
    
    for(int i = 0; i < list.length - 1; i++){
      //iterations++;
      
      int currentMin = list[i];
      int currentMinIndex = i;
      iterations++;
      for(int j = i + 1; j < list.length; j++){
        iterations++;
        if(list[j] < list[i]){
          currentMinIndex = j;
          currentMin = list[j];
        }
        if(currentMinIndex != i){
          int temp = list[i];
          list[i] = list[j];
          list[j] = temp;
          System.out.println(Arrays.toString(list));
        }//System.out.println(Arrays.toString(list));
      }
    }
    //System.out.println(Arrays.toString(list));
    return iterations;
  }//end selection sort
  
}//end class
