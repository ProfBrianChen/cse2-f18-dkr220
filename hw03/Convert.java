//Daniel Reph
//CSE 02 HW03 Convert
//September 15, 2018
//This program is going to convert the amount 
//of rainfall into cubic miles

//begin by importing the scanner
import java.util.Scanner;

public class Convert{
  
    public static void main(String ars[]){
      Scanner myScanner = new Scanner(System.in);
      //print a statement asking the user for data
      System.out.print("Enter the affected area in acres: ");
      //set a variable for the user's input
      double area = myScanner.nextDouble();
      //print a statement asking for the amount of rainfall
      System.out.print("Enter the rainfall in the affected area: ");
      //set a variable for the amount of rain
      double rain = myScanner.nextDouble();
      //convert acres to miles
      double areaMiles = area * .0015625;
      //convert inches to miles
      double rainMiles = rain / 12 / 5280;
      //convert previous answers into cubic miles
      double cubicMiles = areaMiles * rainMiles;
      //print out how many cubic miles the rainfall was
      System.out.println(cubicMiles + " cubic miles");
      

     
     

      
      
      
      
      
      
    }
}