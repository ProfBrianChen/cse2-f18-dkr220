//Daniel Reph
//CSE 02 HW 03 Pyramid
//September 15, 2018

//begin by importing the scanner
import java.util.Scanner;

public class Pyramid{
  
    public static void main(String ars[]){
      Scanner myScanner = new Scanner(System.in);
      //print a statement asking for the length of the side
      System.out.println("The square side of the pyramid is (input length): ");
      //make a variable for the side length
      double side = myScanner.nextDouble();
      //print a statement asking for the height of the pyramid
      System.out.println("The height of the pyramid is (input height): ");
      //make a variable for the height
      double height = myScanner.nextDouble();
      //compute the area of the box
      double area = side * side * height;
      //print a statement for the volume of the pyramid
      //making sure to divide the previous area by 3
      System.out.println("The volume inside the pyramid is " + area/3);
      

    }
}