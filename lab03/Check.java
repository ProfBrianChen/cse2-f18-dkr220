//Daniel Reph
//CSE 02 Check
//September 13, 2018
//This program is going to calculate the check total for each person
//when splitting it evenly
import java.util.Scanner;
  public class Check{   
    public static void main(String ars[]){

    //declare the scanner 
    Scanner myScanner = new Scanner(System.in);
    //ask for the value of the check total
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //the user will input the total here
    double checkCost = myScanner.nextDouble();
    //ask for the percentage they wish to tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; 
    //We want to convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    //declare values for new variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //for storing digits
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);




  
      
      
    }
}
