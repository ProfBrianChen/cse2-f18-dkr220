//////Daniel Reph
/////HW 01, September 2, 2018
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String ars[]){
    ////prints welcome message using Lehigh ID
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-D--K--R--2--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Daniel Reph and I am a junior at Lehigh University majoring in Statistics.");                   
  }
}
