//Daniel Reph
//CSE 002-310
//November 20, 2018
//HW 09: Remove Element
//This program will make an array and then remove elements according to the user
//import arrays, random, and scanner helpers
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class RemoveElements {
    public static void main(String args[]) {
      //delcare and initialize scanner
      Scanner myScanner = new Scanner(System.in);
      //make new arrays
      int num[]=new int[10];
      //make default answer variable yes
      String answer = "y";
      do{//create do while loop
        //assign num array to call randomInput to fill with numbers
        num = randomInput();
        //print out num array
        System.out.println(Arrays.toString(num));
        //create new array size 9
        int newArray[] = new int[9];
        //enter what user wants to remove
        System.out.println("Enter the index");
        //assign value to pos
        int pos = myScanner.nextInt();
        //check to see if in range
        if(pos<-1 || pos>10){
          //if so tell user it's invalid
          System.out.println("Index is not valid");
          //print out original array
          System.out.println("Output array is: " + Arrays.toString(newArray));
          //ask user for target value
          System.out.println("Enter the target value");
          //assign it to targetVal variable
          int targetVal = myScanner.nextInt();
          //make new array with max size 10
          int[] editArray = new int[10];
          //remove targetVal from array calling method
          editArray = remove(num, targetVal);
          //print out new array
          System.out.println("new array: " + Arrays.toString(editArray));
          //ask user if they want to go again or not
          System.out.println("Go again, y or Y for yes, anything else to quit");
          //answer goes into variable
          answer = myScanner.next();
        }//if in range
        else{
          //call method to delete from array
          newArray = delete(num, pos);
          //print out new array
          System.out.println("New array: " + Arrays.toString(newArray));
          //ask user for target val
          System.out.println("Enter the target value");
          //assign it to variable
          int targetVal = myScanner.nextInt();
          //make new array max size 10
          int[] editArray = new int[10];
          //call method to make new array
          editArray = remove(newArray, targetVal);
          //print out new array
          System.out.println("new array: " + Arrays.toString(editArray));
          //ask user to go again or not
          System.out.println("Go again, y or Y for yes, anything else to quit");
          //put into variable
          answer = myScanner.next();
        }
      }//check answer to see if go again or not
      while(answer.equals("y") || answer.equals("Y"));
    }//random input method
  public static int[] randomInput(){
    //initialize random number generator
    Random randomGenerator = new Random();
    //make new array
    int[] inputArray = new int[10];
    //use for loop to fill array
    for(int i = 0; i < 10; i++){
      //fill each spot with random number from 1 to 10
      inputArray[i] = randomGenerator.nextInt(10);
    }//return array to main
    return inputArray;
  }//delete method
  public static int[] delete(int list[], int pos){
    //make new array
    int[] deleteArray = new int[9];
    //make for loop
    for(int i = 0; i < 10; i++){
      //check index with pos, if equal skip over value
      if(i == pos){
      }
      else if(i< pos){//if index less than pos
        //fill with normal array
        deleteArray[i] = list[i];
      }//otherwise fill with i + 1 spot
          else{
            deleteArray[i-1] = list[i];
          }
      }//return array
    return deleteArray;
      
  }//remove method
  public static int[] remove(int inputArray[], int remove){
    //make two counters
    int counter = 0;
    int counter2 = 0;
    //use for loop to count how many times targetVal is found
    for(int i = 0; i < inputArray.length; i++){
      if(inputArray[i] == remove){
        //if found increase counter
        counter++;
      }
    }
    //make new array to size of original array - counter
    int[] editArray = new int[inputArray.length - counter];
    //System.out.println("counter :" + counter);
    //use for loop to fill array
    for(int x = 0; x < inputArray.length - counter; x++){
      if(inputArray[x+counter2] == remove){
        //if input array value equals targetVal increase counter
        counter2++;
        editArray[x] = inputArray[x+counter2];
      }//otherwise fill array with values
      else{
        editArray[x] = inputArray[x+counter2];
      }
    }//return the edited array
    return editArray;   
  }
}
