//Daniel Reph
//CSE 002-310
//November 20, 2018
//HW 09: Linear Search
//This program will input 15 integers into an array and find what the user asks for
//import array helper method, random generator, and scanner
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class CSE2Linear{
  public static void main(String args[]){
    //declare and initialize scanner
    Scanner myScanner = new Scanner(System.in);
    //declare array with size 15
    int[] inputArray = new int[15];
    //declare i and y variables
    int i = 0;
    int y = 0;
    //run while loope 15 times
    while(i < inputArray.length){
      //ask user for integer b/w 1 and 100
      System.out.println("Enter an integer between 1 and 100");
      //boolean integer = myScanner.hasNextInt();
      //take in integer as variable
      double x = myScanner.nextDouble();
      //check to see if integer value
      if(x%1 != 0){
        //tell user it's not
        System.out.println("Not an integer");
      }//check to see within range
      if(x<1 || x > 100){
        //tell user it's not
        System.out.println("Out of range");
      }//check to see if greater than previous variable
        if(x<y){
          //tell user it's not
        System.out.println("Not greater than previous entry");
      }//if none met, then it's good to use
      else if(x%1 == 0 && x>0 && x<101 && x>=y){
        //make x variable an int
        x = (int)x;
        //put it in the spot of the array
        inputArray[i] = (int)x;
        //make x variable new y variable to compare
        y = (int)x;
        //increase array index
        i++;
      }
    }//print out array
    System.out.println("Input Array: " + Arrays.toString(inputArray));
    //ask user for grade to look for in array
    System.out.println("Enter a grade to be searched for");
    //make it the key variable
    int key = myScanner.nextInt();
    //make a count variable, call method
    int count = binarySearch2(inputArray, key);
    //declare new array the calls shuffle method
    int[] shuffleArray = shuffle(inputArray);
    //print out shuffled array
    System.out.println(Arrays.toString(shuffleArray));
    //ask user again to search for grade
    System.out.println("Enter a grade to be searched for");
    //make it key variable again
    key = myScanner.nextInt();
    //make new counter, call method
    int count2 = linearSearch(shuffleArray, key);
    //if return -1, not found, tell user
    if(count2 == -1){
      System.out.println(key + " was not found");
    }//else, good to go
    else{//print out where the value was found   
      System.out.println(key + " was found in " + (count2+1) + " tries");
    }
    
    
  }//end main
  public static int[] shuffle(int list[]){
    //declare number generator
    Random randomGenerator = new Random();
    for(int i = 0; i < 15; i++){
      //make temp variable
      int[] temp = new int[1];
      //get random number
      int x = randomGenerator.nextInt(15);
      //make second temp variable
      int temp2 = list[i];
      //switch arrays 
      list[i] = list[x];
      list[x] = temp2;
    }//print out array
    for(int i = 0; i < 15; i++){
      //System.out.print(list[i] + " ");
      //System.out.print(Arrays.toString(list));
    }
      System.out.println();
      return list;
      }//end shuffle
   public static int binarySearch2(int[] inputArray, int key) {
     //this code is used from Professor Carr's lecture
     //make low and high variables, and counter
		int low = 0;
		int high = inputArray.length-1;
		int counter = 0;
     while(high >= low) {
       //find middle of array
       int mid = (low + high)/2;
       //if key less than mid, make new high
       if (key < inputArray[mid]) {
         high = mid - 1;
         counter++;
       }//or if key is mid, then you found key
       else if (key == inputArray[mid]) {
         counter++;
         System.out.println(key + " was found in " + counter + " tries");
         return mid;
       }//otherwise make new low
       else {
         low = mid + 1;
         counter++;
       }
     }
     System.out.println(key + " was not found");
     return -1;
	}//end binary search
  //make method to do linear search
  public static int linearSearch(int[] shuffleArray, int key){
    //create for loop
    for (int i = 0; i < shuffleArray.length; i++) {
      //go through loop until found, if so return i counter
      if (key == shuffleArray[i]) 
        return i;
    }//if not return -1
    return -1;
  }//end linear search
}//end class
